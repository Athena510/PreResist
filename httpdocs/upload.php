<!DOCTYPE html>
<html>
<b>
You will be redirected to the homepage. If you do not, click <a href="http://preresist.org">here</a>
<?
$img=$_FILES['img'];
$name=$_POST['pname'];
$condition = $_POST['condition'];
$state = $_POST['state'];
$gResponse = $_POST['g-recaptcha-response'];
//echo "$age $name $condition $state $gResponse";
if(isset($_POST['submit'])){
	
 if(!isset($name) or !isset($condition) or !isset($state)) {
	 echo "Empty field!";
 }	
 if($img['name']==''){  
  echo "<h2>An Image Please.</h2>";
 }else{
  $filename = $img['tmp_name'];
  $client_id="e904c4a82f25731";
  $handle = fopen($filename, "r");
  $data = fread($handle, filesize($filename));
  $pvars   = array('image' => base64_encode($data));
  $timeout = 30;
  include('../captcha.php');
  $recap = array(
            'secret' => $captcha_secret,
            'response' => $gResponse
        );
 
$verify = curl_init();
curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
curl_setopt($verify, CURLOPT_POST, true);
curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($recap));
curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($verify);
$decoded = json_decode($response, true);
if(!$decoded['success']) {
	header("Location: index.php?captchaFailed=yes");
	exit();
	//we're done here
}

curl_close($verify);
unset($captcha_secret); //free it
  //IMGUR
  $curl = curl_init();
  
  curl_setopt($curl, CURLOPT_URL, 'https://api.imgur.com/3/image.json');
  curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Client-ID ' . $client_id));
  curl_setopt($curl, CURLOPT_POST, 1);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $pvars);
  $out = curl_exec($curl);
 
  curl_close ($curl);
  $pms = json_decode($out,true);
  if(isset($pms['data']['error'])) {
	echo "\n \n Imgur replied with an error. Are you sure you're uploading an actual image?";
	$errs = fopen('errs.txt', 'a');
	fwrite($errs, $pms['data']['link']);
	fwrite($errs, $pms['data']['error']);
	header("Location: index.php?captchaFailed=yes");
	exit();
  }
  $url=$pms['data']['link'];
  if($url!=""){
   echo "<h2>Thank you for uploading! After approval your image will appear on PreResist!</h2>";
   echo "<img src='$url'/>";
   //Testing database functionality
   include('../db_settings.php');
   $link = mysqli_connect("50.62.209.38:3306", $db_user, $db_pass)
   or die("Could not connect: " . mysql_error());
   
   unset($db_user, $db_pass);
   include('../db_queries.php');
   insert_into_db($link, $name, $url, $condition, 0, $state);
   mysqli_close($link);
   header("Location: index.php?justSubmitted=true");
   exit();
  }else{
   echo "<h2>There's a Problem</h2>";
   header("Location: index.php?captchaFailed=yes");
   echo $pms['data']['error'];  
  } 
 }
}
?>
</b>
</html>