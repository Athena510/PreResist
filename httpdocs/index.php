<!DOCTYPE html>
<html lang="en">


<head>
<title>PreResist</title>
<meta charset="UTF-8">
<link rel="apple-touch-icon" sizes="57x57" href="/images/apple-icon-57x57.png?">
<link rel="apple-touch-icon" sizes="60x60" href="/images/apple-icon-60x60.png?">
<link rel="apple-touch-icon" sizes="72x72" href="/images/apple-icon-72x72.png?">
<link rel="apple-touch-icon" sizes="76x76" href="/images/apple-icon-76x76.png?">
<link rel="apple-touch-icon" sizes="114x114" href="/images/apple-icon-114x114.png?">
<link rel="apple-touch-icon" sizes="120x120" href="/images/apple-icon-120x120.png?">
<link rel="apple-touch-icon" sizes="144x144" href="/images/apple-icon-144x144.png?">
<link rel="apple-touch-icon" sizes="152x152" href="/images/apple-icon-152x152.png?">
<link rel="apple-touch-icon" sizes="180x180" href="/images/apple-icon-180x180.png?">
<link rel="icon" type="image/png" sizes="192x192"  href="/images/android-icon-192x192.png?">
<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png?">
<link rel="icon" type="image/png" sizes="96x96" href="/images/favicon-96x96.png?">
<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png?">
<link rel="icon" type="image/png" href="/images/favicon.ico?">
<link rel="manifest" href="/images/manifest.json?">
<meta name="msapplication-TileColor" content="#ffffff?">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png?">
<meta name="theme-color" content="#ffffff?">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Show Congress what's at stake. Post your preexisting condition!">
<meta name="keywords" content="healthcare,congress,preexisting,condition,ahca">
<!-- Facebook opengraph -->
<meta property="og:url"                content="http://preresist.org/" />
<meta property="og:type"               content="website" />
<meta property="og:title"              content="PreResist" />
<meta property="og:description"        content="Because health care shouldn't be a privilege" />
<meta property="og:image"              content="http://preresist.org/images/fb-logo.png" />

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" type="text/css" href="bttrlazyloading.min.css" />
<style>
	.desc {
		width: 100%;
		min-height:180px;
		display: block;
		max-height: 180px;
		overflow-y: hidden;
		word-break: break-word;
	}
 /*@media screen and (max-height: 575px){ #rc-imageselect, .g-recaptcha {transform:scale(0.5);-webkit-transform:scale(0.5);transform-origin:0 0;-webkit-transform-origin:0 0;} }*/
</style>
</head>

<body class="w3-content" style="max-width:1300px">
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>


<script src="jquery.bttrlazyloading.min.js"></script>
<script>
<!--
$(function() {
	$('.bttrlazyloading').bttrlazyloading();
});
-->
</script>






<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript">
$("#justSubmitted" ).hide(); //hide the slide down banner unless we need it
$('#upload_form').submit(function(){
    $('#submit').addClass('disabled');
});
</script>
<?php

 if(isset($_GET['justSubmitted'])) {
	echo '
	<script>
	$(document).ready(function(){
		$("#justSubmitted").slideToggle("slow", function() {});

	});
	</script>
		';
 } elseif(isset($_GET['captchaFailed'])) {
	echo '
	<script>
	$(document).ready(function(){
		$("#captchaFailed").slideToggle("slow", function() {});

	});
	</script>
		';
 }



?>


<div id="fb-root"></div>
<div id="justSubmitted" style="display:none;">

<div class="w3-green w3-panel w3-display-container">
<h3>Success!</h3>
  <span onclick="this.parentElement.style.display='none'"
  class="w3-button w3-green w3-large w3-display-topright w3-mobile">&times;</span>
<p>Thank you for your submission! It will be reviewed by our site administrators, then it'll be visible!
Together we can make a difference.</p>
</div>
</div>
<div id="captchaFailed" style="display:none;">

<div class="w3-red w3-panel w3-display-container">
<h3>Captcha failed!</h3>
  <span onclick="this.parentElement.style.display='none'"
  class="w3-button w3-red w3-large w3-display-topright w3-mobile">&times;</span>
<p>Oh no! Looks like our spam check failed. Please make sure 'I'm not a robot!' is checked and try again.</p>
</div>
</div>
<div class="w3-row">
 <img src="/images/logo.png" class="w3-image">
</div>
<!-- First Grid: Upload and about -->
<div class="w3-row">

  <div class="w3-half w3-container w3-cell w3-mobile" style="min-height:550px">

    <div class="w3-padding-16 w3-center w3-container w3-large">
      <div class="w3-left-align">
		<p style="color:gray">
        <b style="color:#BB000F">130 million Americans have preexisting health conditions.</b>
        Congress wants to turn back the clock on health care reform.
		Millions could lose coverage.
		Older Americans and those with preexisting conditions could be charged substantially more, putting coverage out of reach.
		For some of us, the consequences could be life and death.
		<br>
		<br>
		We want our elected officials to see the human cost of their decisions – face to face.
		We are not statistics in a Congressional budget report.  We are moms and dads, friends and colleagues, children and elders.
		We are Republicans and Democrats and Independents.
		And we stand together to demand equitable health care coverage for all.
		</p>
		<b style="color:gray"><i>Show Congress what’s at stake. Post your photo and story, then share it. <b style="color:#345694;">#WePreResist</b></b></i>
		<br>
		<br>
		<div class="w3-row" style="min-height:50px">
		<div class="w3-third w3-display-container w3-center" style="height:100%; padding-top: 1px;">
		<div class="fb-like" data-href="https://www.facebook.com/PreResist/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
		</div>
		<div class="w3-quarter w3-display-container w3-center" style="height:100%; padding-top:5px; padding-left:18px;">
		<a href="https://twitter.com/PreResist" class="twitter-follow-button" data-show-count="false">Follow @PreResist</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
		</div>
		<div class="w3-third w3-display-container w3-right w3-hide-small" style="min-height:50px;">
		<i style="color:#345694; font-size:13px;">Media: <a href="mailto:&#105;&#110;&#113;&#117;&#105;&#114;&#105;&#101;&#115;&#064;&#112;&#114;&#101;&#114;&#101;&#115;&#105;&#115;&#116;&#046;&#111;&#114;&#103;">&#105;&#110;&#113;&#117;&#105;&#114;&#105;&#101;&#115;&#064;&#112;&#114;&#101;&#114;&#101;&#115;&#105;&#115;&#116;&#046;&#111;&#114;&#103;</a></i>
		</div>		
		<div class="w3-third w3-display-container w3-center w3-hide-medium w3-hide-large" style="min-height:50px;">
		<i style="color:#345694; font-size:13px;">Media: <a href="mailto:&#105;&#110;&#113;&#117;&#105;&#114;&#105;&#101;&#115;&#064;&#112;&#114;&#101;&#114;&#101;&#115;&#105;&#115;&#116;&#046;&#111;&#114;&#103;">&#105;&#110;&#113;&#117;&#105;&#114;&#105;&#101;&#115;&#064;&#112;&#114;&#101;&#114;&#101;&#115;&#105;&#115;&#116;&#046;&#111;&#114;&#103;</a></i>
		</div>
		</div>
		<br>

      </div>

    </div>
  </div>
  <div class="w3-half w3-container w3-center w3-cell w3-mobile" style="min-height:550px">
    <div class="w3-padding-16 w3-padding-large w3-container">
	  <br>
      <div class="w3-indigo">
      You are more than a number. Make them look you in the eye.
      </div>
	  <form id="upload_form" class="w3-container w3-card-2 w3-padding-16 w3-white" action="upload.php" enctype="multipart/form-data" method="POST">
        <div class="w3-section w3-padding-16">
		  <div class="w3-row">
		  <div class="w3-col s3 w3-display-container" style="position: relative; top:50%; transform: translateY(-50%);">Upload your photo</div>
		  <div class="w3-col s9 w3-display-container" style="position: relative; top:50%; transform: translateY(-50%);">
		  <input class="w3-input w3-middle w3-display-middle" style="width:100%;" name="img" size="35" required type="file">
		  </div>
		  </div>

        </div>
		<div class="w3-section">
		  <div class="w3-row">
		  <div class="w3-col s3" style="position: relative; top:50%; transform: translateY(-50%); min-height:12px;">Name</div>
          <div class="w3-col s9 w3-display-container" style="position: relative; top:50%; transform: translateY(-50%); min-height:12px;">
          <input class="w3-input" style="width:100%;" type="text" required name="pname" maxlength="35">
		  </div>
		  </div>
        </div>
        <div class="w3-section">

          <label>Your preexisting condition and/or comment (140 chars max)</label>

		   <textarea class="w3-input" name="condition" maxlength="140" cols="40" rows="3" required></textarea>
        </div>
		<div class="w3-section">
			<select name = "state" class="w3-input"><option value="XX" selected="selected">What state are you from?</option><option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">District Of Columbia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option><option value="AS">American Samoa</option><option value="GU">Guam</option><option value="MP">Northern Mariana Islands</option><option value="PR">Puerto Rico</option><option value="UM">United States Minor Outlying Islands</option><option value="VI">Virgin Islands</option><option value="AA">Armed Forces Americas</option><option value="AP">Armed Forces Pacific</option><option value="AE">Armed Forces Others</option></select>

        </div>
		<div class="w3-row w3-display-container w3-mobile" >
		<div class="g-recaptcha w3-mobile" data-sitekey="6LdutyMUAAAAAIagYiol0dJH9vEAawRv6t27Qjt-"></div>
		<button type="submit" class="w3-button w3-display-right w3-hide-small w3-hover-blue" name="submit" value="Upload" style="background-color:#345694; color:azure;">Submit</button>
		<button type="submit" class="w3-button w3-mobile w3-hide-medium w3-hide-large w3-hover-blue" name="submit" value="Upload" style="background-color:#345694; color:azure;">Submit</button>

		</div>
	  </form>
    </div>
  </div>

</div>
<div class="w3-row w3-hide-large w3-hide-medium" style="min-height:100px"></div>
<!-- Photos begin here, counter inserted as well -->
<?
   include('../db_settings.php');
   $link = mysqli_connect("50.62.209.38:3306", $db_user, $db_pass)
   or die("Could not connect: " . mysql_error());
   unset($db_user, $db_pass);
   include('../db_queries.php');
   echo '<div class="w3-row w3-display-container w3-border-top w3-hide-small" style="min-height:75px;"><h2>';
   echo '<div class="w3-display-middle" style="padding-top: 15px; padding-bottom: 15px;">';
   echo '<b style="color:#BB000F;">';
   echo count_approved($link);
   echo '</b> <b style="color:#345694;">PREresisting!</b></h2>';
   echo '</div>';
   echo '</div>';
   regenerate_targets($link);
   exit();
?>

</body>
</html>
